package net.guides.springboot2.springboot2jpacrudexample.service;
import java.util.List;
import java.util.Map;
import org.springframework.http.ResponseEntity;

import net.guides.springboot2.springboot2jpacrudexample.exception.ResourceNotFoundException;
import net.guides.springboot2.springboot2jpacrudexample.model.Employee;

public interface EmployeeService {
	
	public Employee getEmployeeById(Long employeeId) throws ResourceNotFoundException;
	
	public Employee createEmployee(Employee employee) throws ResourceNotFoundException;
		
	public Employee updateEmployee( Long employeeId, Employee employeeDetails) throws ResourceNotFoundException;
	
	public Map<String, Boolean> deleteEmployee(Long employeeId) throws ResourceNotFoundException;
		

	public Map<String, Boolean> softdeleteEmployee(Long employeeId) throws ResourceNotFoundException;
	
	public List<Employee> getEmployeeDetails() throws ResourceNotFoundException;
	
			

}
